import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="lAcademy", # Replace with your own username
    version="0.0.1",
    author="R",
    author_email="R@test.com",
    description="To generate complete dataset",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    install_requires=['Click','beautifulsoup4','html5lib','selenium',\
        'youtube_dl','chromedriver_autoinstaller'],
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    entry_points='''
        [console_scripts]
        lAcademy=lAcademy:cli
    ''',
)