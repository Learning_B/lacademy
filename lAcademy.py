from __future__ import unicode_literals
import os
import platform
import re
import sys
import os 
import threading
from pathlib import Path
from urllib.parse import urlparse
import click
import codecs
import configparser
import json
import tempfile
from shutil import copy2
from selenium import webdriver
import requests
import chromedriver_autoinstaller
from urllib.parse import quote
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import time
import youtube_dl

def download_file(folder,chapter_index,name,url):
    ydl_opts = {'cookiefile': 'cookies.txt', 'force_generic_extractor': True,
                    'outtmpl': os.path.join('class',folder,str(chapter_index)+'.'+name.replace('/','_')+ '.%(ext)s'),
                    'sleep_interval': 15, 'retries': 10}
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([url])

def get_info(key):
    homde_dir = os.path.expanduser('~')
    config = configparser.ConfigParser()
    config.read(os.path.join(homde_dir,'.linuxAcademy'))
    return config['info'][key]
def get_username():
    return get_info('user')

def create_dir(name):
    if os.path.isdir(name):
        return
    os.mkdir(name)

def save_content_to_download(content,class_name):
    folder = os.path.join('class',class_name)
    config = configparser.ConfigParser()
    class_info = {}
    for cl in content:
        class_info[cl['name']] = cl['url']
    config[class_name] = class_info
    with open(os.path.join(folder,'class_url'), 'w') as configfile:
        config.write(configfile)

def check_if_already_scrapped(title):
    if os.path.isfile(os.path.join('class',title,'class_url')):
        return True
    return False   

def get_files(folder):
    if os.path.isdir(folder):
        allFiles = [os.path.join(folder,x) for x in os.listdir(folder) if os.path.isfile(os.path.join(folder,x))]
        return allFiles
    return None

def save_to_file(file_name,content):
    with open(file_name, 'wb') as f:
        f.write(content)

def check_cookie_file():
    if os.path.isfile('cookies.txt'):
        date_changed = os.path.getctime('cookies.txt')
        file_path = os.path.abspath('cookies.txt')
        diff_date = (time.time() - date_changed)/86400
        if (int(diff_date)> 1):  click.echo(f'The {click.style(file_path,"red")} doesnot seems to be recent please redownload it using cookies extension in chrome')
    else:
        raise(f'The file cookies.txt is not present')
    return

def scrap_class(title,browser,parsed,temp_folder):
    '''This will scrap all links of the video content from the class syllabus page
    and also download all the pdf if given
    '''
    lessons = []
    click.echo(click.style(f'Working on class: {title}!!!!', fg='green'))
    ## wait till all the syllabus links are present
    WebDriverWait(browser,100).until(
        EC.presence_of_element_located((By.XPATH,"//*[@id='syllabus-tab']/div/div[2]/div/div/div[1]"
        )))
    ## get all the links of all the video
    anchors = parsed.find_all('a', attrs={'class', 'syllabus-item'})
    for anchor in anchors:
        if '/course/' in anchor.get('href'):
            chapter = {}
            chapter['url'] = 'https://linuxacademy.com' + anchor.get('href')
            chapter['name'] = anchor.find('h6').text.replace(':','_')
            lessons.append(chapter)
        else:
            pass
    # save all the links to the class folder
    save_content_to_download(lessons,title)
    click.echo(click.style('Sleeping for 05 seconds...zzzZZZZZ before next scrap', fg='red'))
    ## download the study guide too
    link = parsed.select('html.wf-omnespro-n4-active.wf-omnespro-n6-active.wf-omnespro-i4-active.wf-omnespro-n3-active.wf-omnespro-i3-active.wf-omnespro-i6-active.wf-active body div.section.course-syllabus div.container div.row div.col-md-12 div.tab-content div#study-guides-tab.tab-pane.fade div.row div.col-sm-12.col-md-8.col-md-pull-4 div.row.course-page-study-guides div.study-guide-container.col-xs-12')
    for l in link:
        a_url = l.find('a').get('href')
        if 'pdf' in a_url:
            pdf = 'https://linuxacademy.com' + quote(a_url)
            pdf_title = l.find('h3').text
            browser.get(pdf)
            time.sleep(5)
    # read all the downloaded files that are downloaded to temp folder
    downloaded = get_files(temp_folder)
    folder = os.path.join('class',title)
    ## loop copy the downloaded content to the class folder and remove them from temp
    for file in downloaded:
        copy2(file,folder)
        os.remove(file)
    # lets try the interactive diagrams download
    interactive_diagram_section = parsed.select('#study-guides-tab > div > div.col-sm-12.col-md-8.col-md-pull-4 > div > div.col-md-12 > div')
    for links in interactive_diagram_section:
        name = urlparse(links.find('a').get('href')).path
        browser.get(links.find('a').get('href'))
        time.sleep(10)
        with open(os.path.join('class',title,name.replace('/','_')), 'w') as f:
            f.write(browser.page_source)
    click.echo(click.style('Sleeping for a min seconds...zzzZZZZZ before next scrap', fg='red'))
    time.sleep(60)

@click.group()
def cli():
    pass

@cli.command()
@click.option('--username', prompt=True,
              default=get_username(),
              show_default='current user')
# @click.argument('src', nargs=-1,type=click.Path(),show_default='llsd')
@click.password_option()
def setup(username,password):
    homde_dir = os.path.expanduser('~')
    config = configparser.ConfigParser()
    config['info'] = {'user': username,'password':password}
    with open(os.path.join(homde_dir,'.linuxAcademy'), 'w') as configfile:
        config.write(configfile)

@cli.command()
def my_course():
    '''Will gather the videos from your recent class and download them from later viewing'''
    username = get_username()
    password_user = get_info('password')
    create_dir('class')
    url = 'https://app.linuxacademy.com/dashboard'
    user_agent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 ' \
              'Safari/537.36 '
    headers = {'User-Agent': user_agent}
    with tempfile.TemporaryDirectory() as f:
        ## chrome options
        chrome_options = Options()
        chrome_options.add_experimental_option("prefs",{
            "download.default_directory": f,
            "download.prompt_for_download": False,
            "download.directory_upgrade": True,
        })
        chrome_options.add_argument(f'user-agent={user_agent}')
        chrome_options.add_argument('--disable-extensions')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-popup-blocking')
        chrome_options.add_argument('--disable-logging')
        chrome_options.add_argument('--allow-running-insecure-content')
        ## download chrome tool
        try:
            version = chromedriver_autoinstaller.utils.get_chrome_version()
            major_version = chromedriver_autoinstaller.utils.get_major_version(version)
            package = chromedriver_autoinstaller.utils.get_chromedriver_filename()
            driver = os.path.join(chromedriver_autoinstaller.utils.get_chromedriver_path(),major_version,package)
            browser = webdriver.Chrome(options=chrome_options,executable_path=driver)
        except Exception as e:
            chromedriver_autoinstaller.install()
            browser = webdriver.Chrome(options=chrome_options)

        click.echo(click.style('Let the Work begin', fg='green'))
        browser.set_page_load_timeout(10000)
        browser.maximize_window()
        browser.get("https://linuxacademy.com/")
        WebDriverWait(browser,100).until(
            EC.element_to_be_clickable((
                By.XPATH,"//*[@id='nav']/ul/li[7]/ul/li[2]/a"
            ))
        )
        link = browser.find_element_by_partial_link_text('Log In')
        link.click()
        WebDriverWait(browser, 60).until(
            EC.element_to_be_clickable((By.XPATH,"//*[@id='auth0-lock-container-1']/div/div[2]/form/div/div/div/button")))
        click.echo(click.style('Logging into linux Academy', fg='green'))
        user = browser.find_element_by_name('username')
        user.send_keys(username)
        password = browser.find_element_by_name('password')
        password.send_keys(password_user)
        password.send_keys(Keys.RETURN)
        
        try:
            WebDriverWait(browser,100).until(
                EC.presence_of_element_located((By.XPATH,"//*[@id='navigationUsername']"
                )))
        except:
            click.echo(click.style('LogIn failed', fg='red'))
            sys.exit(1)

        click.echo(click.style('We are logged in!!!!', fg='green'))
        WebDriverWait(browser,100).until(
                EC.element_to_be_clickable((By.XPATH,"//*[@id='cdk-accordion-child-1']/div/button"
                )))
        browser.find_element_by_css_selector('button.mat-flat-button:nth-child(6) > span:nth-child(1) > span:nth-child(1)').click()
        time.sleep(5)
        html = browser.page_source
        parsed_html = BeautifulSoup(html, 'html5lib')
        my_class = parsed_html.select('html body app-root div.view app-dashboard.ng-star-inserted div.viewer div.d-flex.flex-column.flex-lg-row div.main-widgets div app-recent-courses mat-expansion-panel.mat-expansion-panel.ng-tns-c9-2.mat-expanded div#cdk-accordion-child-1.mat-expansion-panel-content.ng-trigger.ng-trigger-bodyExpansion.mat-expanded div.mat-expansion-panel-body')\
                [0].find_all('a')
        for link in my_class:
            each_class_link = link.get('href')
            browser.get(each_class_link)
            WebDriverWait(browser, 60).until(
                EC.element_to_be_clickable(
                    (By.XPATH,"//*[@id='syllabus-tab']/div/div[2]/div/div/div[1]/a[4]")))
            html = browser.page_source
            parsed = BeautifulSoup(html,'html5lib')
            title = parsed.find('div', attrs={'class', 'course-title'}).find('h1').text.split(':')[1]
            create_dir(os.path.join('class',title))
            if check_if_already_scrapped(title): 
                click.echo(click.style(f'Data file for the class : {title} is already present!!!!', fg='red'))
                continue # if file already present
                # will need a better validation
            scrap_class(title,browser,parsed,f)
    browser.close()
    browser.quit()
    click.echo(click.style('All done all classes info is collected', fg='green'))

@cli.command()
def download():
    click.echo(click.style(f'Starting downloads First lets check your 0_o cookies.txt is there or Not!!'))
    check_cookie_file()
    ## lets get the classes 
    all_folders = [x for x in os.listdir('class') if os.path.isdir(os.path.join('class',x))]
    for folder in all_folders:
        if os.path.isfile(os.path.join('class',folder,'.done')): continue
        info_file = os.path.join('class',folder,'class_url')
        config = configparser.ConfigParser()
        config.read(info_file)
        config_section = config.sections()[0]
        threads_list = []
        chapter_index = 1
        for k,v in config[config_section].items():
            x = threading.Thread(target=download_file, args=(folder,chapter_index,k,v,))
            threads_list.append(x)
            x.start()
            chapter_index +=1
        ## thread 
        for t in threads_list:
            t.join()
        Path(os.path.join('class',folder,'.done')).touch()

    
